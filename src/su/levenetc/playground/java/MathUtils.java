package su.levenetc.playground.java;

/**
 * Created by elevenetc on 13/06/15.
 */
public class MathUtils {
	public static float distance(float diffX, float diffY) {
		return (float) Math.sqrt(diffX * diffX + diffY * diffY);
	}
}
