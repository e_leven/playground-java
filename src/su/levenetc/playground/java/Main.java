package su.levenetc.playground.java;

import su.levenetc.playground.java.lang.ChangeStaticFinalField;
import su.levenetc.playground.java.rxjava.CompletableSample;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		ChangeStaticFinalField.run();
	}
}